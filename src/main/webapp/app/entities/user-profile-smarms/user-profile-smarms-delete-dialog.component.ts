import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IUserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';
import { UserProfileSmarmsService } from './user-profile-smarms.service';

@Component({
  selector: 'jhi-user-profile-smarms-delete-dialog',
  templateUrl: './user-profile-smarms-delete-dialog.component.html'
})
export class UserProfileSmarmsDeleteDialogComponent {
  userProfile: IUserProfileSmarms;

  constructor(
    protected userProfileService: UserProfileSmarmsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.userProfileService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'userProfileListModification',
        content: 'Deleted an userProfile'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-user-profile-smarms-delete-popup',
  template: ''
})
export class UserProfileSmarmsDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userProfile }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(UserProfileSmarmsDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.userProfile = userProfile;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/user-profile-smarms', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/user-profile-smarms', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
