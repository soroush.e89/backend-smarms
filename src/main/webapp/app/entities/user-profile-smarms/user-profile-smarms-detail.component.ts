import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IUserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';

@Component({
  selector: 'jhi-user-profile-smarms-detail',
  templateUrl: './user-profile-smarms-detail.component.html'
})
export class UserProfileSmarmsDetailComponent implements OnInit {
  userProfile: IUserProfileSmarms;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ userProfile }) => {
      this.userProfile = userProfile;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
