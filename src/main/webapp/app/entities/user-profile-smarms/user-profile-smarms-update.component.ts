import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IUserProfileSmarms, UserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';
import { UserProfileSmarmsService } from './user-profile-smarms.service';

@Component({
  selector: 'jhi-user-profile-smarms-update',
  templateUrl: './user-profile-smarms-update.component.html'
})
export class UserProfileSmarmsUpdateComponent implements OnInit {
  userProfile: IUserProfileSmarms;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    title: [],
    position: [],
    description: [],
    image: [],
    imageContentType: [],
    backgroundImage: [],
    backgroundImageContentType: [],
    linkedIn: [],
    email: [],
    instagram: [],
    resume: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected userProfileService: UserProfileSmarmsService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ userProfile }) => {
      this.updateForm(userProfile);
      this.userProfile = userProfile;
    });
  }

  updateForm(userProfile: IUserProfileSmarms) {
    this.editForm.patchValue({
      id: userProfile.id,
      name: userProfile.name,
      title: userProfile.title,
      position: userProfile.position,
      description: userProfile.description,
      image: userProfile.image,
      imageContentType: userProfile.imageContentType,
      backgroundImage: userProfile.backgroundImage,
      backgroundImageContentType: userProfile.backgroundImageContentType,
      linkedIn: userProfile.linkedIn,
      email: userProfile.email,
      instagram: userProfile.instagram,
      resume: userProfile.resume
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        if (isImage && !/^image\//.test(file.type)) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      () => console.log('blob added'), // sucess
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const userProfile = this.createFromForm();
    if (userProfile.id !== undefined) {
      this.subscribeToSaveResponse(this.userProfileService.update(userProfile));
    } else {
      this.subscribeToSaveResponse(this.userProfileService.create(userProfile));
    }
  }

  private createFromForm(): IUserProfileSmarms {
    const entity = {
      ...new UserProfileSmarms(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      title: this.editForm.get(['title']).value,
      position: this.editForm.get(['position']).value,
      description: this.editForm.get(['description']).value,
      imageContentType: this.editForm.get(['imageContentType']).value,
      image: this.editForm.get(['image']).value,
      backgroundImageContentType: this.editForm.get(['backgroundImageContentType']).value,
      backgroundImage: this.editForm.get(['backgroundImage']).value,
      linkedIn: this.editForm.get(['linkedIn']).value,
      email: this.editForm.get(['email']).value,
      instagram: this.editForm.get(['instagram']).value,
      resume: this.editForm.get(['resume']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IUserProfileSmarms>>) {
    result.subscribe((res: HttpResponse<IUserProfileSmarms>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
