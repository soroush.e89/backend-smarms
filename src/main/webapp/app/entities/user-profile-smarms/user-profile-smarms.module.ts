import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { SmarmsSharedModule } from 'app/shared';
import {
  UserProfileSmarmsComponent,
  UserProfileSmarmsDetailComponent,
  UserProfileSmarmsUpdateComponent,
  UserProfileSmarmsDeletePopupComponent,
  UserProfileSmarmsDeleteDialogComponent,
  userProfileRoute,
  userProfilePopupRoute
} from './';

const ENTITY_STATES = [...userProfileRoute, ...userProfilePopupRoute];

@NgModule({
  imports: [SmarmsSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    UserProfileSmarmsComponent,
    UserProfileSmarmsDetailComponent,
    UserProfileSmarmsUpdateComponent,
    UserProfileSmarmsDeleteDialogComponent,
    UserProfileSmarmsDeletePopupComponent
  ],
  entryComponents: [
    UserProfileSmarmsComponent,
    UserProfileSmarmsUpdateComponent,
    UserProfileSmarmsDeleteDialogComponent,
    UserProfileSmarmsDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SmarmsUserProfileSmarmsModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
