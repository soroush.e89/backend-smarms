import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';
import { UserProfileSmarmsService } from './user-profile-smarms.service';
import { UserProfileSmarmsComponent } from './user-profile-smarms.component';
import { UserProfileSmarmsDetailComponent } from './user-profile-smarms-detail.component';
import { UserProfileSmarmsUpdateComponent } from './user-profile-smarms-update.component';
import { UserProfileSmarmsDeletePopupComponent } from './user-profile-smarms-delete-dialog.component';
import { IUserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';

@Injectable({ providedIn: 'root' })
export class UserProfileSmarmsResolve implements Resolve<IUserProfileSmarms> {
  constructor(private service: UserProfileSmarmsService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IUserProfileSmarms> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<UserProfileSmarms>) => response.ok),
        map((userProfile: HttpResponse<UserProfileSmarms>) => userProfile.body)
      );
    }
    return of(new UserProfileSmarms());
  }
}

export const userProfileRoute: Routes = [
  {
    path: '',
    component: UserProfileSmarmsComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'smarmsApp.userProfile.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: UserProfileSmarmsDetailComponent,
    resolve: {
      userProfile: UserProfileSmarmsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smarmsApp.userProfile.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: UserProfileSmarmsUpdateComponent,
    resolve: {
      userProfile: UserProfileSmarmsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smarmsApp.userProfile.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: UserProfileSmarmsUpdateComponent,
    resolve: {
      userProfile: UserProfileSmarmsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smarmsApp.userProfile.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const userProfilePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: UserProfileSmarmsDeletePopupComponent,
    resolve: {
      userProfile: UserProfileSmarmsResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'smarmsApp.userProfile.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
