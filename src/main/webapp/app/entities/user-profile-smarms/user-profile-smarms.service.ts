import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IUserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';

type EntityResponseType = HttpResponse<IUserProfileSmarms>;
type EntityArrayResponseType = HttpResponse<IUserProfileSmarms[]>;

@Injectable({ providedIn: 'root' })
export class UserProfileSmarmsService {
  public resourceUrl = SERVER_API_URL + 'api/user-profiles';

  constructor(protected http: HttpClient) {}

  create(userProfile: IUserProfileSmarms): Observable<EntityResponseType> {
    return this.http.post<IUserProfileSmarms>(this.resourceUrl, userProfile, { observe: 'response' });
  }

  update(userProfile: IUserProfileSmarms): Observable<EntityResponseType> {
    return this.http.put<IUserProfileSmarms>(this.resourceUrl, userProfile, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUserProfileSmarms>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUserProfileSmarms[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
