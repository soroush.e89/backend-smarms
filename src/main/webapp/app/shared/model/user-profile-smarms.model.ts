export interface IUserProfileSmarms {
  id?: number;
  name?: string;
  title?: string;
  position?: string;
  description?: string;
  imageContentType?: string;
  image?: any;
  backgroundImageContentType?: string;
  backgroundImage?: any;
  linkedIn?: string;
  email?: string;
  instagram?: string;
  resume?: string;
}

export class UserProfileSmarms implements IUserProfileSmarms {
  constructor(
    public id?: number,
    public name?: string,
    public title?: string,
    public position?: string,
    public description?: string,
    public imageContentType?: string,
    public image?: any,
    public backgroundImageContentType?: string,
    public backgroundImage?: any,
    public linkedIn?: string,
    public email?: string,
    public instagram?: string,
    public resume?: string
  ) {}
}
