import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SmarmsSharedLibsModule, SmarmsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [SmarmsSharedLibsModule, SmarmsSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [SmarmsSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SmarmsSharedModule {
  static forRoot() {
    return {
      ngModule: SmarmsSharedModule
    };
  }
}
