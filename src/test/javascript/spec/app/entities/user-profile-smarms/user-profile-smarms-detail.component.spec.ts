/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SmarmsTestModule } from '../../../test.module';
import { UserProfileSmarmsDetailComponent } from 'app/entities/user-profile-smarms/user-profile-smarms-detail.component';
import { UserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';

describe('Component Tests', () => {
  describe('UserProfileSmarms Management Detail Component', () => {
    let comp: UserProfileSmarmsDetailComponent;
    let fixture: ComponentFixture<UserProfileSmarmsDetailComponent>;
    const route = ({ data: of({ userProfile: new UserProfileSmarms(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SmarmsTestModule],
        declarations: [UserProfileSmarmsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(UserProfileSmarmsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(UserProfileSmarmsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.userProfile).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
