/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { SmarmsTestModule } from '../../../test.module';
import { UserProfileSmarmsUpdateComponent } from 'app/entities/user-profile-smarms/user-profile-smarms-update.component';
import { UserProfileSmarmsService } from 'app/entities/user-profile-smarms/user-profile-smarms.service';
import { UserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';

describe('Component Tests', () => {
  describe('UserProfileSmarms Management Update Component', () => {
    let comp: UserProfileSmarmsUpdateComponent;
    let fixture: ComponentFixture<UserProfileSmarmsUpdateComponent>;
    let service: UserProfileSmarmsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [SmarmsTestModule],
        declarations: [UserProfileSmarmsUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(UserProfileSmarmsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(UserProfileSmarmsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(UserProfileSmarmsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserProfileSmarms(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new UserProfileSmarms();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
