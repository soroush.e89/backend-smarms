/* tslint:disable max-line-length */
import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { of } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { UserProfileSmarmsService } from 'app/entities/user-profile-smarms/user-profile-smarms.service';
import { IUserProfileSmarms, UserProfileSmarms } from 'app/shared/model/user-profile-smarms.model';

describe('Service Tests', () => {
  describe('UserProfileSmarms Service', () => {
    let injector: TestBed;
    let service: UserProfileSmarmsService;
    let httpMock: HttpTestingController;
    let elemDefault: IUserProfileSmarms;
    let expectedResult;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(UserProfileSmarmsService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new UserProfileSmarms(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'image/png',
        'AAAAAAA',
        'image/png',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', async () => {
        const returnedFromService = Object.assign({}, elemDefault);
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a UserProfileSmarms', async () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .create(new UserProfileSmarms(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a UserProfileSmarms', async () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            title: 'BBBBBB',
            position: 'BBBBBB',
            description: 'BBBBBB',
            image: 'BBBBBB',
            backgroundImage: 'BBBBBB',
            linkedIn: 'BBBBBB',
            email: 'BBBBBB',
            instagram: 'BBBBBB',
            resume: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of UserProfileSmarms', async () => {
        const returnedFromService = Object.assign(
          {
            name: 'BBBBBB',
            title: 'BBBBBB',
            position: 'BBBBBB',
            description: 'BBBBBB',
            image: 'BBBBBB',
            backgroundImage: 'BBBBBB',
            linkedIn: 'BBBBBB',
            email: 'BBBBBB',
            instagram: 'BBBBBB',
            resume: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign({}, returnedFromService);
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a UserProfileSmarms', async () => {
        const rxPromise = service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
